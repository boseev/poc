package com.boseev.chain.smartcontract.service;

import com.boseev.chain.smartcontract.model.client.Client;
import com.boseev.chain.smartcontract.model.customer.Customer;
import com.google.common.base.Preconditions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.tuples.generated.Tuple3;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContractServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContractServiceTest.class);

    @Autowired
    private ContractService contractService;

    @Test
    public void contractLifeCycle() throws Exception {
        Customer newCustomer =
                new Customer("id_1", "PiggyBank")
                        .setWalletPassword("123456")
                        .setWalletPath("wallet/UTC--2018-04-01T19-12-37.648000000Z--ef974805759bfd054babfd3e080b5ab56c7c3009.json");

        String clientContractNumber = null;

        // initialize the wallet
        Credentials wallet = getWallet(newCustomer.getWalletPath(), newCustomer.getWalletPassword());

        try {
            // create new contract
            clientContractNumber = contractService.deployNewContract(wallet, new Client("Super", "Man"));
            LOGGER.info("New contract {}", clientContractNumber);

            // create new agreement
            long agreementId = contractService.addAgreement(wallet, clientContractNumber, "Car insurance", new BigDecimal(20_000));
            LOGGER.info("New agreementId {} for contract {}", agreementId, clientContractNumber);

            // retrieve agreement info
            Tuple3<String, String, BigInteger> agreementDetails = contractService.retrieveAgreement(wallet, clientContractNumber, agreementId);
            LOGGER.info("Retrieve agreement details {} for contract {}", agreementDetails, clientContractNumber);
        } finally {
            // do cleanup
            if (clientContractNumber != null) {
                contractService.killContract(wallet, clientContractNumber);
            }
        }
    }


    private Credentials getWallet(String walletPath,
                                  String walletPassword) throws IOException, CipherException {
        URL walletUrl = ContractServiceTest.class.getClassLoader().getResource(walletPath);
        Preconditions.checkNotNull(walletUrl, "Wallet is not found by path {}", walletPath);
        File file = new File(walletUrl.getFile());
        return WalletUtils.loadCredentials(
                walletPassword,
                file);
    }

}
