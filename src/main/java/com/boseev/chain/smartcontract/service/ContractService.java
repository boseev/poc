package com.boseev.chain.smartcontract.service;

import com.boseev.chain.smartcontract.model.client.Client;
import com.boseev.chain.smartcontract.sample.model.InsuranceContract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tuples.generated.Tuple3;
import org.web3j.tx.Contract;
import org.web3j.tx.ManagedTransaction;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Service
public class ContractService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContractService.class);
    private final Web3j web3j;

    @Autowired
    public ContractService(Web3j web3j) {
        this.web3j = web3j;
    }


    /**
     * Deploy a new contract
     *
     * @param client
     * @return contractId
     * @throws Exception
     */
    public String deployNewContract(Credentials wallet, Client client) throws Exception {

        LOGGER.info("Adding a new contract for the wallet {}", wallet.getAddress());

        RemoteCall<InsuranceContract> deploy = InsuranceContract.deploy(
                web3j,
                wallet,
                ManagedTransaction.GAS_PRICE,
                Contract.GAS_LIMIT,
                client.getFirstName(),
                client.getLastName()
        );

        return deploy.send().getContractAddress();
    }


    /**
     * Add new Agreement
     *
     * @param wallet
     * @param contractNumber
     * @param agreementName
     * @param amount
     *
     * @return agreementId
     * @throws Exception
     */
    public long addAgreement(Credentials wallet, String contractNumber, String agreementName,  BigDecimal amount) throws Exception {
        InsuranceContract load = InsuranceContract.load(
                contractNumber
                , web3j
                , wallet
                , ManagedTransaction.GAS_PRICE,
                Contract.GAS_LIMIT);

        RemoteCall<TransactionReceipt> newAgreement = load.newAgreement(wallet.getAddress(), agreementName, BigInteger.valueOf(amount.longValue()));
        TransactionReceipt sendInfo = newAgreement.send();
        LOGGER.info("Agreement info {}", sendInfo);

        List<InsuranceContract.NewAgreementEventResponse> events = load.getNewAgreementEvents(sendInfo);
        return events.get(0).eventId.longValue();
    }


    /**
     * Retrieve agreement details
     *
     * @param wallet
     * @param contractNumber
     * @param agreementId
     * @return
     * @throws Exception
     */
    public Tuple3<String, String, BigInteger> retrieveAgreement(Credentials wallet, String contractNumber, long agreementId) throws Exception {
        InsuranceContract load = InsuranceContract.load(
                contractNumber
                , web3j
                , wallet
                , ManagedTransaction.GAS_PRICE,
                Contract.GAS_LIMIT);

        Tuple3<String, String, BigInteger> send = load.getAgreements(BigInteger.valueOf(agreementId)).send();
        LOGGER.info("Retrieved agreement {}", send);
        return send;
    }


    /**
     * Kill the contract
     *
     * @param wallet
     * @param contractNumber
     * @throws Exception
     */
    public void killContract(Credentials wallet, String contractNumber) throws Exception {
        LOGGER.info("Kill contract {}", contractNumber);
        InsuranceContract load = InsuranceContract.load(
                contractNumber
                , web3j
                , wallet
                , ManagedTransaction.GAS_PRICE,
                Contract.GAS_LIMIT);
        load.kill();
    }


}
