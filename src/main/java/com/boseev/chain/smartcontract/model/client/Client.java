package com.boseev.chain.smartcontract.model.client;

import java.util.ArrayList;
import java.util.List;

/**
 * Client information which stores in MongoDb
 */
public class Client {
    private final String firstName;
    private final String lastName;
    /**
     * Some identification data e.g. passport, drivers licence etc
     * Photo.
     * Date of birth, place of living.
     *
     * Then the same info will store in Etherium contract it will help us avoid fraud.
     *
     * Also the data could be changed then we have to update all the contracts
     *
     */

    /**
     * Contract info
     */
    private List<ClientContract> contracts = new ArrayList<>();

    public Client(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void addContract(ClientContract contract) {
        this.contracts.add(contract);
    }



}
