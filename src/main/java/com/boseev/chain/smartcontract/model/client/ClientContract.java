package com.boseev.chain.smartcontract.model.client;

import com.google.common.base.MoreObjects;

import java.time.LocalDateTime;

/**
 * Client contract information
 */
public class ClientContract {
    private final String contractType;
    private final String ethAddress;
    private final String description;
    private LocalDateTime addTimestamp = LocalDateTime.now();
    private LocalDateTime updateTimestamp;

    /**
     * Customer.id
     */
    private final String ownerId;

    public ClientContract(String contractType, String ethAddress, String description, String ownerId) {

        this.contractType = contractType;
        this.ethAddress = ethAddress;
        this.description = description;
        this.ownerId = ownerId;
    }

    public String getContractType() {
        return contractType;
    }

    public String getEthAddress() {
        return ethAddress;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getAddTimestamp() {
        return addTimestamp;
    }

    public ClientContract setAddTimestamp(LocalDateTime addTimestamp) {
        this.addTimestamp = addTimestamp;
        return this;
    }

    public LocalDateTime getUpdateTimestamp() {
        return updateTimestamp;
    }

    public ClientContract setUpdateTimestamp(LocalDateTime updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
        return this;
    }

    public String getOwnerId() {
        return ownerId;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("contractType", contractType)
                .add("ethAddress", ethAddress)
                .add("description", description)
                .add("addTimestamp", addTimestamp)
                .add("updateTimestamp", updateTimestamp)
                .add("ownerId", ownerId)
                .toString();
    }
}
