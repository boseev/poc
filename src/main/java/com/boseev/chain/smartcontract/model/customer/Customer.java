package com.boseev.chain.smartcontract.model.customer;

import com.google.common.base.MoreObjects;

/**
 * Any corporation who resposible for adding and querying data
 */
public class Customer {
    private final String id;
    private final String name;

    private String walletPath;
    private String walletPassword;

    public Customer(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getWalletPath() {
        return walletPath;
    }

    public Customer setWalletPath(String walletPath) {
        this.walletPath = walletPath;
        return this;
    }

    public String getWalletPassword() {
        return walletPassword;
    }

    public Customer setWalletPassword(String walletPassword) {
        this.walletPassword = walletPassword;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("walletPath", walletPath)
                .add("walletPassword", walletPassword)
                .toString();
    }
}
