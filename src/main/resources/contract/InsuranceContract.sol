pragma solidity ^0.4.2;

contract InsuranceContract {

    struct Client {
         string firstName;
         string lastName;
    }

    struct Agreement {
             address beneficiary;
             string description;
             uint amount;
    }

    address owner;
    Client holder;

    Agreement[] agreements;
    event NewAgreement(uint eventId);

     function kill() { if (msg.sender == owner) suicide(owner); }

    function InsuranceContract(string _clientFirstName, string _clientLastName) public {
        owner = msg.sender;
        holder =  Client(_clientFirstName,  _clientLastName);
    }

     function newAgreement(address beneficiary, string description, uint amount)  public {
        agreements.push(Agreement(beneficiary, description, amount));
        NewAgreement(agreements.length - 1);
     }

     function getAgreementCount() constant returns (uint) {
        return agreements.length;
     }

     function getAgreements(uint agreementId) constant returns (
                                              address beneficiary,
                                              string description,
                                              uint amount
     ) {
        beneficiary = agreements[agreementId].beneficiary;
        description = agreements[agreementId].description;
        amount = agreements[agreementId].amount;
     }



}


